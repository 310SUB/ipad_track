DECLARE 
m varchar2(1000);
sd date;
ed date;
oppty varchar2(500);
us_state varchar2(10);
us_city varchar2(200);
us_street varchar2(500);
us_zip number;
status varchar2(50);
author_email VARCHAR2(100);
dv_mr VARCHAR2(100);
dv_rm VARCHAR2(100);
comm VARCHAR2(1000);
create_on date;
create_by VARCHAR2(150);
update_on date;
update_by VARCHAR2(150);
l_body      CLOB;
l_body_html CLOB;


BEGIN

select start_date, 
       end_date, 
       opportunity, 
       state, 
       city, 
       street, 
       zip, 
       req_status, 
       email, 
       DELIVERYCODE_MR, 
       DELIVERYCODE_RM, 
       comments, 
       created_on, 
       created_by, 
       updated_on, 
       updated_by 
into   sd, 
       ed, 
       oppty, 
       us_state, 
       us_city, 
       us_street, 
       us_zip, 
       status, 
       author_email, 
       dv_mr, 
       dv_rm, 
       comm, 
       create_on, 
       create_by, 
       update_on, 
       update_by 
 from reservations 
  where id=:p33_ID;

l_body := 'To view the content of this message, please use an HTML enabled mail client.'||utl_tcp.crlf;

l_body_html := '<html>
                      <head>
                        <style type="text/css">
                          body{font-family: Arial, Helvetica, sans-serif;
                               font-size:10pt;
                               margin:30px;
                               background-color:#ffffff;}

                          span.sig{font-style:italic;
                                   font-weight:bold;
                                   color:#811919;}
                        </style>
                      </head>
                      <body>'||utl_tcp.crlf;
    
l_body_html := l_body_html ||'Request # <b>' ||:p33_ID||'</b> has been modified. New data: <br> <br>'||utl_tcp.crlf;
l_body_html := l_body_html ||'Asset Requester: '||author_email||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html ||'START DATE: '||sd||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html||'END DATE: '||ed||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html||'Request Status: '||status||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html||'Opportunity: '||oppty||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html||'Delivery address: '||us_zip||' '||us_street||' '||us_city||' '||us_state||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html||'Outbound Tracking Number: '||dv_mr||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html||'Inbound Tracking Number: '||dv_rm||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html||'Updated on: '||update_on||'<br />'||utl_tcp.crlf;
l_body_html := l_body_html||'Updated_by: '||update_by||'<br />'||utl_tcp.crlf;
l_body_html := l_body_html||'More details about this request <a href="https://apex.oraclecorp.com/pls/apex/f?p=11308:33:::::P33_ID:'||:p33_ID||'"> here </a>'||'<br />'||'<br />'||utl_tcp.crlf||utl_tcp.crlf;
    l_body_html := l_body_html ||'  Sincerely,<br />'||utl_tcp.crlf||utl_tcp.crlf;
    l_body_html := l_body_html ||'  <span class="sig">The Asset Tracker Dev Team</span><br />'||utl_tcp.crlf;


select RTRIM(XMLAGG(XMLELEMENT(e,email || ',')).EXTRACT('//text()'),',') email into m from users where role='Administrator';
htmldb_mail.Send(p_to   => 'malina.ciosnar@oracle.com,andrei.iosub@oracle.com',--author_email,
                     p_from => :app_user,
                     p_body      => l_body,
                     p_body_html => l_body_html,
                     p_subj => 'Asset Request '||:p33_ID|| ' Modified ');

END;