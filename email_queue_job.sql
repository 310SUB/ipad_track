BEGIN
 
DBMS_SCHEDULER.DROP_JOB (
   job_name                => 'EMAIL_QUEUE_SEND',
   force                   => FALSE
);    

DBMS_SCHEDULER.CREATE_JOB (
   job_name           =>  'EMAIL_QUEUE_SEND',
   job_type           =>  'STORED_PROCEDURE',
   job_action         =>  'EMAIL_QUEUE_PKG.SEND',
   start_date         =>  sysdate,
   repeat_interval    =>  'FREQ=HOURLY; INTERVAL=1', /*every hour*/
   end_date           =>  null,
   auto_drop          =>   FALSE,
   job_class          =>  'DEFAULT_JOB_CLASS',
   comments           =>  'Send email from EMAIL_QUEUE table hourly'
);

END;
/