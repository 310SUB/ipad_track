/*
    Date created: 16.03.2016
    Created by: AIB
    
    these procedures will insert data into the EMAIL_QUEUE table - one row for one email         
    
    
    select * from user_SCHEDULER_JOBS
    
     should return EMAIL_QUEUE_SEND
    
*/

create or replace package email_queue_pkg as

  g_typ_res_rmrndr      emailq_link.email_type%type := 'RESERVATION END DATE REMINDER';
  g_typ_res_new         emailq_link.email_type%type := 'NEW RESERVATION REQUEST';
  g_typ_res_received    emailq_link.email_type%type := 'ASSET_RECEIVED';
  g_typ_res_upd         emailq_link.email_type%type := 'RESERVATION UPDATED';


procedure build_email (
      p_type             in  varchar2
    , p_pk               in  number
    , p_send_now         in  varchar2 default null --Y                   
);

/*
    procedure send
    this procedure looks for emails that should be sent and sends them using apex_mail   
*/

procedure send;

end  email_queue_pkg;