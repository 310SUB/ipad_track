DECLARE 
m varchar2(1000);
l_body      CLOB;
l_body_html CLOB;


BEGIN

l_body := 'To view the content of this message, please use an HTML enabled mail client.'||utl_tcp.crlf;

l_body_html := '<html>
                      <head>
                        <style type="text/css">
                          body{font-family: Arial, Helvetica, sans-serif;
                               font-size:10pt;
                               margin:30px;
                               background-color:#ffffff;}

                          span.sig{font-style:italic;
                                   font-weight:bold;
                                   color:#811919;}
                        </style>
                      </head>
                      <body>'||utl_tcp.crlf;
    
l_body_html := l_body_html ||'Request # <b>' ||'</b> has been CREATED. New data: <br> <br>'||utl_tcp.crlf;
l_body_html := l_body_html ||'Asset Requester: '||:app_user||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html ||'START DATE: '||:p29_start_date||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html||'END DATE: '||:p29_end_date||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html||'Request Status: <b>NEW</b><br>'||utl_tcp.crlf;
l_body_html := l_body_html||'Opportunity: '||:p29_opportunity||'<br>'||utl_tcp.crlf;
l_body_html := l_body_html||'Delivery address: '||:p29_zip||' '||:p29_street||' '||:p29_city||' '||:p29_state||'<br><br>'||utl_tcp.crlf;
l_body_html := l_body_html||'More details about this request <a href="https://apex.oraclecorp.com/pls/apex/f?p=11308:32:::::"> here </a>'||'<br />'||'<br />'||utl_tcp.crlf||utl_tcp.crlf;
    l_body_html := l_body_html ||'  Sincerely,<br />'||utl_tcp.crlf||utl_tcp.crlf;
    l_body_html := l_body_html ||'  <span class="sig">The Asset Tracker Dev Team</span><br />'||utl_tcp.crlf;


select RTRIM(XMLAGG(XMLELEMENT(e,email || ',')).EXTRACT('//text()'),',') email into m from users where role='Administrator' or role='Asset_manager';
htmldb_mail.Send(p_to   => 'malina.ciosnar@oracle.com,andrei.iosub@oracle.com',
                     p_from => :app_user,
                     p_body      => l_body,
                     p_body_html => l_body_html,
                     p_subj => 'Asset Request has been created ');

END;