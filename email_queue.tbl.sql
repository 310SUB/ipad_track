/*
undo create table
*/

drop table emailq_link;
drop table email_queue;
drop sequence email_queue_seq;


create table EMAIL_QUEUE (
	 id							number				
	,mail_id					number			
	,mail_subj					varchar2(2000)	
	,mail_to					varchar2(2000)						
	,mail_from					varchar2(2000)							
	,mail_replyto				varchar2(2000)							
	,mail_cc					varchar2(2000)							
	,mail_bcc					varchar2(2000)						
	,mail_body					clob							
	,mail_body_html				clob							
	,mail_send_date				date
	,mail_send_count			number						
	,mail_send_error			varchar2(4000)
	,last_updated_by			varchar2(255)
	,last_updated_on			date
	,mail_message_created		date
	,mail_message_created_by	varchar2(255)	
  --constraints 
  
  , 	constraint 	 emailq$pk  primary key (id)
);
/

COMMENT ON COLUMN email_queue.mail_send_date IS 'what is the earliest date when this email should be sent'; 
/





CREATE SEQUENCE  email_queue_seq  MINVALUE 10000 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 10000 NOCACHE  NOORDER  NOCYCLE  NOPARTITION ;
/


create or replace trigger email_queue_biu
before INSERT or UPDATE on email_queue
for each row
begin

	if inserting then
		if :new.id is null then 
      		:new.id := email_queue_seq.nextval;
  		end if;
	  :new.mail_message_created := sysdate;
	  :new.mail_message_created_by := nvl(v('APP_USER'),'nobody');
  
  elsif updating then

  	:new.last_updated_by := nvl(v('APP_USER'),'nobody');
  	:new.last_updated_on := sysdate;

  end if;

end;
/


create table emailq_link (
		email_queue_id 	number
	,	link_table	    varchar2(50)
	, 	link_pk			number	
	,   email_type		varchar2(100)
  
  --constraints 
  , constraint   eml$fk1 foreign key (email_queue_id) 	references email_queue(id)	  
);
/


COMMENT ON COLUMN emailq_link.link_table IS 'RESERVATIONS or EBA_ASSET_ASSETS or whatever table'; 
/

COMMENT ON COLUMN emailq_link.link_pk IS 'value of the primary key'; 
/