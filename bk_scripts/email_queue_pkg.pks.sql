/*
    Date created: 16.03.2016
    Created by: AIB
    
    these procedures will insert data into the EMAIL_QUEUE table - one row for one email         
    
    
    select * from user_SCHEDULER_JOBS
    
     should return EMAIL_QUEUE_SEND
    
*/

create or replace package email_queue_pkg as



procedure send_new_reservation (p_reservation_id in reservations.id%type);

procedure send_asset_received (p_reservation_id in reservations.id%type);

/*
  procedure gen_reservation_rmndrs;
  
  for each reservation we need to send an email 2 days before the reservation end date
  to the person the asset is assigned to so that he knows he needs to return the asset

*/
procedure gen_reservation_rmndrs (p_reservation_id in reservations.id%type);



/*
    procedure send
    this procedure looks for emails that should be sent and sends them using apex_mail   
*/

procedure send;

end  email_queue_pkg;