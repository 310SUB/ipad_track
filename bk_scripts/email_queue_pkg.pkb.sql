create or replace package body email_queue_pkg as
/*
    Date created: 16.03.2016
    Created by: AIB
    
    these procedures will insert data into the EMAIL_QUEUE table - one row for one email         
*/


  g_typ_res_rmrndr      emailq_link.email_type%type := 'RESERVATION END DATE REMINDER';
  g_typ_res_new         emailq_link.email_type%type := 'NEW RESERVATION REQUEST';
  g_typ_res_received    emailq_link.email_type%type := 'ASSET_RECEIVED';

  cursor crsr_reservation (cp_reservation_id in reservations.id%type) is
      select 
         eba_asset_assets.id                  as asset_id
       , eba_asset_assets.asset_name
       , eba_asset_assets.description         as asset_descr
       , reservations.id                      as reservation_id
       , reservations.email                   as reserved_by
       , reservations.start_date
       , reservations.end_date
       , reservations.end_date - 2            as rmndr_date
       , reservations.state	
       
       , reservations.state                   as send_to_state
       , reservations.city	                  as send_to_city
       , reservations.street                  as send_to_street
       , reservations.zip                     as send_to_zip
       	
       
       , reservations.deliverycode_rm         as inbound_track_no
       
       , reservations.istate                  as return_to_state
       , reservations.icity                   as return_to_city
       , reservations.istreet                 as return_to_street
       , reservations.izip                    as return_to_zip
       
       , reservations.opportunity
       , users_mngr.id                        as user_mngr_id
       , users_mngr.email                     as user_mngr_email
       , users_mngr.state                     as user_mngr_state
       , users_mngr.city                      as user_mngr_city
       , users_mngr.street                    as user_mngr_street
       , users_mngr.zip                       as user_mngr_zip
       , users_mngr.phone                     as user_mngr_phone
      from reservations
      inner join eba_asset_assets on (
        reservations.id_asset = eba_asset_assets.id
      )
      left outer join users users_mngr on (
        reservations.id_manager = users_mngr.id
      )
      where reservations.id = cp_reservation_id;
  
  row_crsr_reservation crsr_reservation%rowtype;
  

g_html_h varchar2(32000) := '<html>
<head>
  <style type="text/css">
    body{font-family: Arial, Helvetica, sans-serif;
         font-size:10pt;
         margin:30px;
         background-color:#ffffff;}

    span.sig{font-style:italic;
             font-weight:bold;
             color:#811919;}
             
table {
    color: #333; 
    font-family: Helvetica, Arial, sans-serif;
    width: 640px;
    border-collapse:
    collapse; border-spacing: 0;
}

td, th { border: 1px solid #CCC; height: 30px; text-align:left; } 

th {
    background: #F3F3F3; 
    font-weight: bold; 
}

td {
    background: #FAFAFA;     
}             
             
             
  </style>
</head>
<body>';
  
  
  g_html_f varchar2(32000) := '<br/><br/>Sincerely,<br />
  <span class="sig">The Asset Tracker Dev Team</span><br />';
  


procedure build_email (
      p_type             in  varchar2
    , p_pk               in  number
    , pout_id            out email_queue.id%type                 
)
is
  
   row_email_queue     email_queue%rowtype;
   
   function draw_product_html
   return varchar2
   is
       l_return varchar2(32000);
   begin
   
        l_return    := l_return || '<table>';
        l_return    := l_return || '<tr><th>Asset Name</th>             <td>'||row_crsr_reservation.asset_name||'</td></tr>';
        l_return    := l_return || '<tr><th>Asset Requester</th>        <td>'||row_crsr_reservation.reserved_by||'</td></tr>';
        l_return    := l_return || '<tr><th>Start Date</th>             <td>'||row_crsr_reservation.start_date||'</td></tr>';
        l_return    := l_return || '<tr><th>End Date</th>               <td>'||row_crsr_reservation.end_date||'</td></tr>';        
        l_return    := l_return || '<tr><th>Opportunity</th>            <td>'||row_crsr_reservation.opportunity||'</td></tr>';
        l_return    := l_return || '<tr><th>Delivery address</th>       <td>'||row_crsr_reservation.send_to_zip||' '||row_crsr_reservation.send_to_street||' '||row_crsr_reservation.send_to_city||' '||row_crsr_reservation.send_to_state||'</td></tr>';
        l_return    := l_return || '</table><br/>';
   
   return l_return;
   
   
   end draw_product_html;
   
   

begin

open  crsr_reservation (cp_reservation_id => p_pk);
loop
    fetch crsr_reservation into row_crsr_reservation; 
    exit when crsr_reservation%notfound;

  if ( p_type = g_typ_res_rmrndr)  then
  

        
        row_email_queue.mail_subj         := 'The Asset Tracker Notification: Asset '|| row_crsr_reservation.asset_name || ' : please return it the day after tomorrow';
        row_email_queue.mail_to           := 'malina.ciosnar@oracle.com,andrei.iosub@oracle.com';--row_crsr_reservation.reserved_by ;
        row_email_queue.mail_from         := 'no-reply@oracle.com'/*row_crsr_reservation.user_mngr_email*/ ;
        row_email_queue.mail_replyto      := null ;
        row_email_queue.mail_body         := 'This is to remind you that you need to return the asset '|| row_crsr_reservation.asset_name || ' by '|| row_crsr_reservation.end_date;
        row_email_queue.mail_body_html    := g_html_h ||'This is to remind you that you need to return the asset '|| row_crsr_reservation.asset_name || ' by '|| row_crsr_reservation.end_date||g_html_f;
        row_email_queue.mail_send_date    := row_crsr_reservation.rmndr_date;
        
  
  
  
      --dbms_output.put_line('generating row for reservation id = '||p_pk);
  
  
  elsif  (p_type = g_typ_res_new) then

        
        row_email_queue.mail_subj         := 'The Asset Tracker Notification: New reservation request for asset '|| row_crsr_reservation.asset_name || ' ';
        row_email_queue.mail_to           := 'malina.ciosnar@oracle.com,andrei.iosub@oracle.com';--row_crsr_reservation.reserved_by ;
        row_email_queue.mail_from         := 'no-reply@oracle.com'/*row_crsr_reservation.user_mngr_email*/ ;
        row_email_queue.mail_replyto      :=  null;
        row_email_queue.mail_body         := 'Request # has been created for '|| row_crsr_reservation.asset_name;
        
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || g_html_h;
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || draw_product_html;
        
       /* row_email_queue.mail_body_html    := row_email_queue.mail_body_html || '<table>';
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || '<tr><th>Asset Requester</th>        <td>'||row_crsr_reservation.reserved_by||'</td></tr>';
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || '<tr><th>Start Date</th>             <td>'||row_crsr_reservation.start_date||'</td></tr>';
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || '<tr><th>End Date</th>               <td>'||row_crsr_reservation.end_date||'</td></tr>';
        --row_email_queue.mail_body_html    := row_email_queue.mail_body_html || '<tr><th>Request Status</th>         <td>'||row_crsr_reservation.start_date||'</td></tr>';
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || '<tr><th>Opportunity</th>            <td>'||row_crsr_reservation.opportunity||'</td></tr>';
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || '<tr><th>Delivery address</th>       <td>'||row_crsr_reservation.send_to_zip||' '||row_crsr_reservation.send_to_street||' '||row_crsr_reservation.send_to_city||' '||row_crsr_reservation.send_to_state||'</td></tr>';
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || '</table><br/>';
        */
        
        
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || 'More details about this request <a href="https://apex.oraclecorp.com/pls/apex/f?p=11308:32:::::"> here </a><br/>';
        
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || g_html_f;
                
        row_email_queue.mail_send_date    := sysdate;       
  
  elsif  (p_type = g_typ_res_received) then    
        
        
        row_email_queue.mail_subj         := 'The Asset Tracker Notification: Asset '|| row_crsr_reservation.asset_name || ' : Asset received.';
        row_email_queue.mail_to           := 'malina.ciosnar@oracle.com,andrei.iosub@oracle.com';--row_crsr_reservation.user_mngr_email ;
        row_email_queue.mail_from         := 'no-reply@oracle.com'/*row_crsr_reservation.reserved_by */ ;
        row_email_queue.mail_replyto      := null ;
        row_email_queue.mail_body         := 'This is to notify you that the requestor has received the asset '|| row_crsr_reservation.asset_name || ' on '|| sysdate;                
        
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || g_html_h;        
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || 'This is to notify you that the requestor has received the asset '|| row_crsr_reservation.asset_name || ' on '|| sysdate || ' <br/><br/>';        
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || draw_product_html;                        
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || 'More details about this request <a href="https://apex.oraclecorp.com/pls/apex/f?p=11308:31:::::"> here </a><br/>';        
        row_email_queue.mail_body_html    := row_email_queue.mail_body_html || g_html_f;
        
        
        row_email_queue.mail_send_date    := sysdate;          


  end if; --p_type = 'RESERVATION END DATE REMINDER'
  
        
end loop;
close crsr_reservation;


  insert into  email_queue values row_email_queue returning id into pout_id;

end build_email;


/*
call me please:

begin
    email_queue_pkg.send_new_reservation (p_reservation_id=>xxx);       
end;

*/

procedure send_new_reservation (p_reservation_id in reservations.id%type)
is
    
     row_emailq_link emailq_link%rowtype;

begin

    build_email (
          p_type           => g_typ_res_new
        , p_pk             => p_reservation_id
        , pout_id          => row_emailq_link.email_queue_id 
    ); 
        
    row_emailq_link.email_type   := g_typ_res_new;
    
    insert into emailq_link values row_emailq_link;
    
    --send all mails  in queue right now
    send;


end send_new_reservation;

/*
call me please:

begin
    email_queue_pkg.send_asset_received (p_reservation_id=>xxx);       
end;

*/

procedure send_asset_received (p_reservation_id in reservations.id%type)
is
    
     row_emailq_link emailq_link%rowtype;

begin

    build_email (
          p_type           => g_typ_res_received
        , p_pk             => p_reservation_id
        , pout_id          => row_emailq_link.email_queue_id 
    ); 
        
    row_emailq_link.email_type   := g_typ_res_received;
    
    insert into emailq_link values row_emailq_link;
    
    --send all mails  in queue right now
    send;


end send_asset_received;

/*
  procedure gen_reservation_rmndrs;
  
  for each reservation we need to send an email 2 days before the reservation end date
  to the person the asset is assigned to so that he knows he needs to return the asset
  
  call me:
  
  begin
      email_queue_pkg.gen_reservation_rmndrs(p_reservation_id => 123131);
  end;

*/
procedure gen_reservation_rmndrs (p_reservation_id in reservations.id%type)
is

  cursor crsr_data is
    select 
        reservations.id as reservation_id
      from reservations      
      where reservations.id not in  (
        select emailq_link.link_pk
          from emailq_link
         where link_table   = 'RESERVATIONS'
           and email_type   = g_typ_res_rmrndr
      )
      and reservations.id = p_reservation_id;
      
   row_data        crsr_data%rowtype;      
      
   row_emailq_link emailq_link%rowtype;


begin

  row_emailq_link.link_table := 'RESERVATIONS';


  open   crsr_data;
  loop
    fetch crsr_data into row_data;
    exit when crsr_data%notfound;
    
    row_emailq_link.link_pk             := row_data.reservation_id;
    
    
    --INSERT ROW INTO email_queue TABLE
    
    build_email (
          p_type           => g_typ_res_rmrndr
        , p_pk             => row_data.reservation_id
        , pout_id          => row_emailq_link.email_queue_id 
    ); 
        
    row_emailq_link.email_type   := g_typ_res_rmrndr;
    
    insert into emailq_link values row_emailq_link;
    
    
    
  end loop;
  close  crsr_data;
  
end gen_reservation_rmndrs;


/*
    procedure send
    this procedure looks for emails that should be sent and sends them using apex_mail   
    
    
    call me:
    
    begin
      email_queue_pkg.send;
    end;
    
*/

procedure send 
is
    cursor crsr_data is 
      select  	 
         email_queue.id						
        ,email_queue.mail_id				
        ,email_queue.mail_subj				
        ,email_queue.mail_to				
        ,email_queue.mail_from				
        ,email_queue.mail_replyto			
        ,email_queue.mail_cc				
        ,email_queue.mail_bcc				
        ,email_queue.mail_body				
        ,email_queue.mail_body_html			
        ,email_queue.mail_send_date			
        ,email_queue.mail_send_count		
        ,email_queue.mail_send_error		
        ,email_queue.last_updated_by		
        ,email_queue.last_updated_on		
        ,email_queue.mail_message_created	
        ,email_queue.mail_message_created_by
       from email_queue
      where email_queue.mail_id is null --mail was not sent
        and email_queue.mail_send_date < sysdate;
        
    row_data crsr_data%rowtype;        
      
      
    l_id NUMBER;  
      
begin

    open  crsr_data;
    loop
        fetch crsr_data into row_data; 
        exit when crsr_data%notfound;
        
       row_data.mail_id :=
           APEX_MAIL.SEND(
                p_to        => row_data.mail_to,
                p_from      => row_data.mail_from,
                p_subj      => row_data.mail_subj,
                p_body      => row_data.mail_body,
                p_body_html => row_data.mail_body_html
            );
        
        if (row_data.mail_id is not null) then
        
            update email_queue
               set 
                       email_queue.mail_id         = row_data.mail_id                  
                   ,   email_queue.mail_send_count = row_data.mail_send_count + 1  
                   
             where email_queue.id = row_data.id;
              
        end if;
        
    end loop;
    close crsr_data;

end send;



end  email_queue_pkg;